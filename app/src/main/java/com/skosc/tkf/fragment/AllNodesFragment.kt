package com.skosc.tkf.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.skosc.tkf.R
import com.skosc.tkf.TKFApplication
import com.skosc.tkf.adapter.NodeRecyclerAdapter
import com.skosc.tkf.viewmodel.AllNodesViewModel
import com.skosc.tkf.viewmodel.AllNodsViewModelFactory
import kotlinx.android.synthetic.main.fragment_all_nodes.*
import org.kodein.di.direct
import org.kodein.di.generic.instance


class AllNodesFragment : Fragment() {
    val vm by lazy {
        val app = activity!!.application as TKFApplication
        val factory: AllNodsViewModelFactory = app.kodein.direct.instance()
        return@lazy ViewModelProviders.of(this, factory).get(AllNodesViewModel::class.java)
    }

    val navController by lazy { findNavController(recycler) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_all_nodes, container, false)
    }

    override fun onStart() {
        super.onStart()


        fab.setOnClickListener { view ->
            onAddNodeClicked()
        }

        recycler.layoutManager = LinearLayoutManager(context)
        val adapter = NodeRecyclerAdapter {
            navController.navigate(
                R.id.nav_mod_node, bundleOf(
                    "rootId" to it.id
                )
            )
        }
        recycler.adapter = adapter
        itemSwiper.attachToRecyclerView(recycler)

        vm.nodes.observe(this, Observer {
            adapter.submitItems(it)
        })

    }


    private fun onAddNodeClicked() {
        MaterialDialog(context!!)
            .title(text = "Add Node")
            .input { dialog, text -> vm.addNode(text.toString()) }
            .positiveButton(text = "Add")
            .show()
    }

    private val itemSwiper = ItemTouchHelper(
        object : ItemTouchHelper.SimpleCallback(
            0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val pos = viewHolder.adapterPosition
                val node = vm.nodes.value!![pos]
                vm.removeNode(node)
            }
        })
}
