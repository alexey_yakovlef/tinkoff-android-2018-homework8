package com.skosc.tkf.adapter

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.skosc.tkf.room.Node
import com.skosc.tkf.R

class NodeRecyclerAdapter(private val onClick: (Node) -> Unit) : RecyclerView.Adapter<NodeRecyclerAdapter.ViewHolder>() {
    val differ = AsyncListDiffer<Node>(this, NodeDifferCallback)

    fun submitItems(items: List<Node>) {
        differ.submitList(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_node, parent, false)
        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val node = differ.currentList[position]
        holder.bind(node)
    }

    override fun getItemCount(): Int = differ.currentList.size

    class ViewHolder(private val view : View, private val onClick: (Node) -> Unit) : RecyclerView.ViewHolder(view) {
        private val text = view.findViewById<TextView>(R.id.node_text)
        private val card = view.findViewById<CardView>(R.id.node_card)

        fun bind(node: Node) {
            text.text = node.value
            card.setOnClickListener { onClick(node) }
            bindToColor(node)
        }

        fun bindToColor(node: Node) {
            if (!node.hasChildren && !node.hasParents) {
                card.background = color(R.color.node_no_rel)
            }

            if (node.hasChildren && !node.hasParents) {
                card.background = color(R.color.node_has_children)
            }

            if (!node.hasChildren && node.hasParents) {
                card.background = color(R.color.node_has_parents)
            }

            if (node.hasChildren && node.hasParents) {
                card.background = color(R.color.node_has_both)
            }
        }

        fun color(@ColorRes res: Int): ColorDrawable {
            val color = ContextCompat.getColor(view.context, res)
            return ColorDrawable(color)
        }
    }
}

private object NodeDifferCallback : DiffUtil.ItemCallback<Node>() {
    override fun areContentsTheSame(oldItem: Node, newItem: Node): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areItemsTheSame(oldItem: Node, newItem: Node): Boolean {
        return areContentsTheSame(oldItem, newItem)
    }
}